/// <reference types="cypress" />
require('cypress-xpath')
describe("Exercício de Automação Web", () => {


  it("", () => {

    cy.visitNoLoading('https://wejump-automation-test.github.io/qa-test/')

  });

  it('Teste deve clicar nos botões "One","Two" e "Four" e checar sua ausência', () => {
    
    cy.get("#btn_one").click();
    cy.get("#btn_two").click();
    cy.get("#btn_link").click();

    cy.get("#btn_one").should("not.be.visible");
    cy.get("#btn_two").should("not.be.visible");
    cy.get("#btn_link").should("not.be.visible");

    cy.get('#reset_buttons').click()

  });

  it('Teste deve clicar nos botões "One","Two" e "Four" dentro do painel IFRAME BUTTONS e checar sua ausência', () => {
    cy.xpath("//*[@src='buttons.html']").then(function ($ele) {
      var buttonOne = $ele.contents().find("#btn_one");
      var buttonTwo = $ele.contents().find("#btn_two");
      var buttonFour = $ele.contents().find("#btn_link");
      var reset = $ele.contents().find("#reset_buttons");

      cy.wrap(buttonOne).click();
      cy.wrap(buttonTwo).click();
      cy.wrap(buttonFour).click();

      cy.wrap(buttonOne).should("not.be.visible");
      cy.wrap(buttonTwo).should("not.be.visible");
      cy.wrap(buttonFour).should("not.be.visible");

      cy.wrap(reset).click();
  
    });
  });

  it('Teste deve preencher o campo "YourFirstName", clicar no botão "One", checar "OptionThree", selecione a opção "ExampleTwo" na select box, e validar a presença da imagem do logo do "Selenium Webdriver"', () => {
    cy.get('#first_name').type('Cypress')
    cy.get('#reset_buttons').click()
    cy.get("#btn_one").click();
    cy.get('#opt_three').check()
    cy.get('#select_box').select('ExampleTwo')
    cy.get('[alt="selenium"]').should('be.visible')
  });
});

