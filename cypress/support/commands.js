
Cypress.Commands.add('visitNoLoading', (link) => {
    cy.on('fail', (e, runnable) => {
        console.log('error', e)
        console.log('runnable', runnable)
        if (e.name === 'error CypressError' &&
          e.message.includes('Timed out after waiting `1000ms` for your remote page to load.')) {
  
          return false
        }
      })
      cy.visit(link,{timeout:10000})
})

